    var queryJSON = this.createMetricQQuery(this.startTime - timeMargin,
                                            this.stopTime + timeMargin,
                                            maxDataPoints,
                                            nonErrorProneMetrics,
                                            ["min", "max", "avg", "count"]);
    if(queryJSON)
    {
      var req = new XMLHttpRequest();
      req.open("POST", METRICQ_BACKEND, true);
      req.setRequestHeader("Content-Type", "application/json; charset=utf-8");
      req.onreadystatechange = function(selfReference, paramMetricArr) { return function(evtObj) {
        selfReference.handleMetricResponse(selfReference, paramMetricArr, evtObj);
      }; }(this, nonErrorProneMetrics);
      req.send(queryJSON);
    }
