Vue.component("configuration-popup", {
  "props": ["config"],
  "data": function() {
    return {
      "popupTitle": "Globale-Einstellungen"
    }
  },
  "computed": {
    "uiResolution": {
      cache: false,
      get: function() {
        return 30 - window.MetricQWebView.instances[0].configuration.resolution;
      },
      set: function(newValue) {
        window.MetricQWebView.instances[0].configuration.resolution = 30 - newValue;
        this.$emit("update:uiResolution", newValue);
      }
    },
    "uiZoomSpeed": {
      cache: false,
      get: function() {
        return window.MetricQWebView.instances[0].configuration.zoomSpeed;
      },
      set: function(newValue) {
        window.MetricQWebView.instances[0].configuration.zoomSpeed = newValue;
        this.$emit("update:uiZoomSpeed", newValue);
      }
    }
  }
});
