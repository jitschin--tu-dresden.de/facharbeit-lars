  parseResponse(parsedJson, paramMetricsArr)
  {
    let tracesAll = new Object();
    let metricBase = undefined;
    let metricAggregate = undefined;
    for(let i = 0; i < parsedJson.length; ++i)
    {
      if(-1 < parsedJson[i].target.indexOf("/")
      && parsedJson[i]["datapoints"]
      && parsedJson[i].datapoints[0])
      {
        metricBase = parsedJson[i].target.substring(0, parsedJson[i].target.indexOf("/"));
        metricAggregate = parsedJson[i].target.substring(parsedJson[i].target.indexOf("/") + 1);

        let parsedTrace = this.parseTrace(metricBase, metricAggregate, parsedJson[i].datapoints);
        if(parsedTrace)
        {
          if(!tracesAll[metricBase])
          {
            tracesAll[metricBase] = new Object();
          }
          tracesAll[metricBase][metricAggregate] = parsedTrace;
        }
      }
    }
    this.processTracesArr(tracesAll);
  }
